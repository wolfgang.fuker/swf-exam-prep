package MSE.SWF.exam.schnittstelle;

import MSE.SWF.exam.Konferenzraum;
import MSE.SWF.exam.Nachricht;
import MSE.SWF.exam.Status;

import java.util.List;

/**
 * Created by FUKERW on 17.04.2016.
 */
public interface GUI {
    public void joinRoom(Konferenzraum room);
    public void leaveRoom(Konferenzraum room);
    public List<Konferenzraum> getAllSubscribedRooms();

    /**
     * Nachricht an alle Teilnehmer in einem Raum
     * @param nachricht
     * @param room
     */
    public void sendMessage(Nachricht nachricht, Konferenzraum room);

    /**
     * Anzeigen einer Nachricht in einem Raum
     * @param nachricht
     * @param room
     */
    public void displayMessage(Nachricht nachricht, Konferenzraum room);

    /**
     * Holt sich über den Raum alle Teilnehmer in diesem Raum
     * und dort jeweils den Namen + Status
     */
    public void showAllTeilnehmerInRoom(Konferenzraum room);

    /**
     * eigenen Status ändern.
     * @param status
     */
    public void changeStatus(Status status);


}
