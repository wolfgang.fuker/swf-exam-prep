package MSE.SWF.exam;

/**
 * Created by FUKERW on 17.04.2016.
 */
public class Header {
    private Teilnehmer teilnehmer;
    private String betreff;

    public Teilnehmer getTeilnehmer() {
        return teilnehmer;
    }

    public void setTeilnehmer(Teilnehmer teilnehmer) {
        this.teilnehmer = teilnehmer;
    }

    public String getBetreff() {
        return betreff;
    }

    public void setBetreff(String betreff) {
        this.betreff = betreff;
    }

    public Header(Teilnehmer teilnehmer, String betreff) {
        this.teilnehmer = teilnehmer;
        this.betreff = betreff;
    }
}
