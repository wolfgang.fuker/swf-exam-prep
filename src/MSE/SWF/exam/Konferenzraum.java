package MSE.SWF.exam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FUKERW on 17.04.2016.
 */
public class Konferenzraum {
    List<Teilnehmer> teilnehmerList;

    public Konferenzraum() {
        teilnehmerList = new ArrayList<>();
    }

    public void addTeilnehmer(Teilnehmer teilnehmer) {
        this.teilnehmerList.add(teilnehmer);
    }

    public void removeTeilnehmer(Teilnehmer teilnehmer) {
        this.teilnehmerList.remove(teilnehmer);
    }

    public void sendMessage(Nachricht nachricht, Teilnehmer fromTeilnehmer) {
        // überprüfen ob fromTeilnehmer in TeilnehmerListe ist.
        // Ja -> Nachricht an alle Teilnehmer in dem Raum senden außer fromTeilnehmer
        // Nein -> BusinessException werfen.
    }
}
