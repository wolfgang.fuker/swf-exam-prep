package MSE.SWF.exam;

/**
 * Created by FUKERW on 15.04.2016.
 */
public class Nachricht {
    private Header header;
    private String body;
    private Anhang anhang;

    public Nachricht(Teilnehmer teilnehmer, String betreff, String text, Anhang anhang) {
        this.header = new Header(teilnehmer, betreff);
        this.body = text;
        this.anhang = anhang;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Anhang getAnhang() {
        return anhang;
    }

    public void setAnhang(Anhang anhang) {
        this.anhang = anhang;
    }
}
