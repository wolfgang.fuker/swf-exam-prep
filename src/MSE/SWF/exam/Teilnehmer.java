package MSE.SWF.exam;

/**
 * Created by FUKERW on 15.04.2016.
 */
public class Teilnehmer {

    private String name;
    private Adresse adresse;
    private Status status;

    public Teilnehmer(Status status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
