package MSE.SWF.exam;

/**
 * Created by FUKERW on 15.04.2016.
 */
public interface Adresse {
    void sendNachricht(Nachricht nachricht);

    /**
     * Setzen / wechslen der Transportübertragung.
     * @param transportmittel
     */
    public void setTransportmittel(Transportmittel transportmittel);

    public Transportmittel getTransportmittel();
}
