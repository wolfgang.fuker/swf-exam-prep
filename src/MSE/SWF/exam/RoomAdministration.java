package MSE.SWF.exam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FUKERW on 17.04.2016.
 */
public class RoomAdministration {

    private RoomAdministration roomAdministration;
    private List<Konferenzraum> roomList;

    private RoomAdministration() {
        roomList = new ArrayList<>();
    }

    /**
     * Singleton Implementierung
     * @return
     */
    public RoomAdministration getInstance() {
        if(roomAdministration == null) {
            roomAdministration = new RoomAdministration();
        }
        return roomAdministration;
    }

    public void addRoom(Konferenzraum konferenzraum) {
    }

    public void removeRoom(Konferenzraum konferenzraum) {

    }

    public List<Konferenzraum> getRoomList() {
        return roomList;
    }

}
